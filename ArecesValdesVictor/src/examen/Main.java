package examen;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int longitudCadena = 9;
		RobotColocador rc1 = new RobotColocador();
		RobotEmpaquetador re1 = new RobotEmpaquetador(1);
		RobotEmpaquetador re2 = new RobotEmpaquetador(2);
		RobotEmpaquetador re3 = new RobotEmpaquetador(3);
		CadenaMontaje cadena = new CadenaMontaje(longitudCadena);
		
		rc1.run();
		re1.start();
		re2.start();
		re3.start();
		
		try {
			re1.join();
			re2.join();
			re3.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
