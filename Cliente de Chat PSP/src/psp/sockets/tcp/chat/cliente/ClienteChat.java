package psp.sockets.tcp.chat.cliente;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;


public class ClienteChat extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JLabel lblHostMsg;
	private JLabel lblNick;
	private JTextField txtHostMsg;
	private JTextField txtNick;
	private JTextField txtInfo;
	private JTextArea txtMensajes;
	private JButton cmdConexion;
	private JButton cmdSalir;
	private Component separador;
	
	private Conexion conexion = null;

	public ClienteChat() {
		super("Chat PSP");
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new MiWindowAdapter());
		setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
		setPreferredSize(new Dimension(900, 600));
		getRootPane().setBorder(new EmptyBorder(20, 20, 20, 20));
		
		/* panel izquierdo */
		JPanel marco = (JPanel) add(new JPanel());
		marco.setLayout(new BoxLayout(marco, BoxLayout.PAGE_AXIS));
		marco.setBorder(new CompoundBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED), new EmptyBorder(10, 10, 10, 10)));
		JPanel panel = (JPanel) marco.add(new JPanel());
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		/* etiquetas y cuadros de texto de Host y Nick */
		marco = new JPanel();
		marco.setLayout(new BoxLayout(marco, BoxLayout.X_AXIS));
		marco.add(lblHostMsg = new JLabel("Host "));
		marco.add(txtHostMsg = crearTextField(true, new MiKeyAdapter()));
		marco.add(separador = Box.createRigidArea(new Dimension(10, 0)));
		marco.add(lblNick = new JLabel("Nick "));
		marco.add(txtNick = crearTextField(true, null));
		panel.add(marco);

		panel.add(Box.createRigidArea(new Dimension(0, 5)));
		
		/* cuadro de texto para los mensajes recibidos*/
		panel.add(new JScrollPane(txtMensajes = new JTextArea()));
		txtMensajes.setEditable(false);
		txtMensajes.setFocusable(false);
		
		panel.add(Box.createRigidArea(new Dimension(0, 5)));
		
		/* cuadro de texto para información del estado del cliente */
		panel.add(txtInfo = crearTextField(false, null));
		txtInfo.setText("Desconectado");
		txtInfo.setFocusable(false);
		/* fin panel izquierdo */
		
		add(Box.createRigidArea(new Dimension(10, 0)));
		
		/* panel derecho: botones de conexión y salir */
		panel = new JPanel(new GridLayout(3,1));
		cmdConexion = new JButton("Conectar");
		cmdConexion.setActionCommand("BC");
		cmdConexion.addActionListener(this);
		cmdSalir = new JButton("Salir");
		cmdSalir.setActionCommand("BS");
		cmdSalir.addActionListener(this);
		marco = new JPanel();
		marco.setBorder(new CompoundBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED), new EmptyBorder(10, 10, 10, 10)));
//		marco.setBorder(new LineBorder(Color.GRAY, 1));
        panel.add(cmdConexion);
		panel.add(Box.createVerticalGlue());
		panel.add(cmdSalir);
		marco.add(panel);
		add(marco);
		/* fin panel derecho */
	}

	private JTextField crearTextField(boolean editable, KeyAdapter keyAdapter) {
		JTextField text;
		text = new JTextField();
		text.setEditable(editable);
		if (keyAdapter != null)
			text.addKeyListener(keyAdapter);
		text.setMargin(new Insets(5, 5, 5, 5));
		text.setMaximumSize(new Dimension(text.getMaximumSize().width, text.getMinimumSize().height));
		return text;
	}
	
	private boolean conectar() {
		try {
			Conexion conexion = new Conexion(lblHostMsg.getText(), lblNick.getText());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Conectar con el servidor mediante socket TCP en el puerto 44444.
		//      Retornar true si se establece la conexión, false en caso contrario.
		
		return true;
	}
	
	private void desconectar() {
		// TODO Desconectar del servidor
	}
	
	private boolean confirmarDesconexion() {
		if (JOptionPane.showConfirmDialog(this, "¿Finalizar conexión y Salir?", "Cerrando la aplicación",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			desconectar();
			return true;
		}
		return false;
	}
	
	private boolean estaConectado() {
		return conexion != null;
	}
	
	private void cerrar() {
		if (!estaConectado() || confirmarDesconexion())
			System.exit(0);
		else
			txtHostMsg.requestFocusInWindow();
	}
	
	private class MiWindowAdapter extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			cerrar();
		}
	}
	
	private class MiKeyAdapter extends KeyAdapter {
		@Override
		public void keyTyped(KeyEvent e) {
			if (e.getComponent() == txtHostMsg && cmdConexion.getActionCommand().equals("BD") && e.getKeyChar() == KeyEvent.VK_ENTER) {
				// TODO Enviar mensaje al servidor
				txtHostMsg.setText("");
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("BC")) { /*  Se ha pulsado el botón Conectar */
			if (conectar()) {
				separador.setVisible(false);
				lblNick.setVisible(false);
				txtNick.setVisible(false);
				lblHostMsg.setText(txtNick.getText() + ": ");
				txtHostMsg.setText("");
				txtNick.setText("");
				cmdConexion.setText("Desconectar");
				cmdConexion.setActionCommand("BD");
				txtHostMsg.requestFocusInWindow();
			}
		}
		else if (e.getActionCommand().equals("BD")) { /* Se ha pulsado el botón Desconectar */
			if (JOptionPane.showConfirmDialog(this, "¿Finalizar conexión?", "Desconexion",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				desconectar();
				separador.setVisible(true);
				lblNick.setVisible(true);
				txtNick.setVisible(true);
				lblHostMsg.setText("Host ");
				txtHostMsg.setText("");
				cmdConexion.setText("Conectar");
				cmdConexion.setActionCommand("BC");
				txtHostMsg.requestFocusInWindow();
			}
		}
		else if (e.getActionCommand().equals("BS")) { /* Se ha pulsado el botón Salir */
			cerrar();
		}
	}

	public static void main(String args[]) {
		ClienteChat frame = new ClienteChat();
		frame.pack();
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
	}
	
}