package psp.sockets.tcp.chat.cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Conexion extends Thread {
	
	private Socket socket;
	private BufferedReader reader;
	private PrintWriter writer;
	private String nick;
	
	public Conexion(String host, String nick) throws IOException {
		try {
			socket = new Socket(host, 4444);
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new PrintWriter(socket.getOutputStream(), true); 
			// si no está en true no envia realmente los mensajes al servidor, habria que hacer flush
			
			/*
			 * Finalizar conexion segun el protocolo de conxion de la capa
			 * de aplicacion
			 */
			writer.print("#nick: " + nick);
			socket.setSoTimeout(5000);
			String respuesta = reader.readLine();
			socket.setSoTimeout(0);
			
		} catch (IOException e) {
			try {
				if (reader != null) {
					reader.close();
				}
				if (writer != null) {
					writer.close();
				}
				if (socket != null) {
					socket.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			throw e;
		}
	}
	@Override
	public void run() {
		
	}
	
}
