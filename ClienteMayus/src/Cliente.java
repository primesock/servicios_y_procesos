import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class Cliente {
	    
    public static void main(String[] args) {
    	
    	boolean seguir = true;
        Socket cliente = null;
        BufferedReader entrada = null;
        PrintWriter salida = null;
        
        try {            

            while(seguir){
            	
                cliente = new Socket("localhost", 4000);//abre socket
                System.out.println("Cliente: Iniciado");  

                //Para leer lo que envie el servidor      
                entrada = new BufferedReader( new InputStreamReader(cliente.getInputStream()));                
                //para imprimir datos del servidor
            	salida = new PrintWriter(new OutputStreamWriter(cliente.getOutputStream()), true);
                //Para leer lo que escriba el usuario            
                BufferedReader brRequest = new BufferedReader(new InputStreamReader(System.in));            
                System.out.println("Cliente: Esperando respuesta de usuario");                
                //captura comando escrito por el usuario
                String request = brRequest.readLine();                
                //manda peticion al servidor
                salida.println(request);
                //captura respuesta e imprime
                String respuesta = entrada.readLine();
                if(request.equals("*")){//terminar aplicacion
                    seguir = false;                  
                    System.out.println("Cliente: Fin de programa");  
                }
                else {
                	System.out.println(respuesta);
                }
            }//end while                                    
        } catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (entrada != null) {
				try {
					entrada.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (salida != null) {
				salida.close();
			}
			if (cliente != null) {
				try {
					cliente.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
