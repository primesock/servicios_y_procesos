#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

/* Primer programa crea el fifo y lee del fifo
 * El segundo programa escribe en el fifo */
int main(void) {
    int p, fp;
    char buffer[100];
    
    p = mknod("FIFO2", S_IFIFO | 0666,0);
    
    if(p == -1){
        printf("HA OCURRIDO UN ERROR...\n");
        return -1;
    }
    fp = open("FIFO0", 0);
    system("./PruebaFIFO2");
    printf("Obteniendo informacion...\n");
    read(p, buffer, sizeof(buffer));
    printf("%d\n", buffer);
    fflush(stdout);
    close(fp);

    return 0;

}
