#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

/* Primer programa crea el fifo y lee del fifo
 * El segundo programa escribe en el fifo */
int main(void) {
    int  fp;
    char buffer[100];
    
    fp = open("FIFO0", O_WRONLY);
    printf("Obteniendo informacion...\n");
    write(fp, buffer, sizeof(buffer));
    printf("%d\n", buffer);
    fflush(stdout);
    close(fp);

    return 0;

}
