#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(void) {
    int pid;
    printf("Estoy en el proceso padre %d\n", getpid());
    printf("Creo el proceso hijo\n");
    pid = fork();
    if (pid == -1){
        printf("Error al crear el nuevo proceso\n");
        return -1;
    }
    if (pid == 0){
         printf("Estoy en el hijo %d\n", getpid());
        printf("El PID de mi padre es %d\n", getppid());
    } else{
        wait(NULL);
        printf("Estoy en el padre %d\n" , getpid());
    }
    return 0;
}
