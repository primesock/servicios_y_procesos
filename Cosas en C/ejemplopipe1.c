#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <string.h>

int main(void){
    pid_t pid;
    int fd[2];
    char buffer[100];
    size_t return_pipe;
    size_t return_write;
    char cadena[] = "Cadena prueba";
    size_t lencadena = strlen(cadena);

    return_pipe = pipe(fd);

    if (return_pipe == 0){
        printf("Pipe creado correctamente\n");
    } else{
        printf("Algo esta fallando\n");
        return -1;
    }

    pid = fork();

    switch (pid) {
        case -1:
            return -1;
        case 0: /* ESTOY EN EL HIJO */
            printf("El hijo escribe en la tuberia..\n");
            printf("La cadena es %s\n", cadena);
            lencadena = strlen(cadena);
            printf("La cadena ocupa %d bytes\n", lencadena);
            if (lencadena > 0) {
                write(fd[1], cadena, lencadena);
            }
            else {
                printf("Error");
            }
            printf("Tamaño return_write en case 0: %u\n", lencadena);
            break;
        default: /* ESTOY EN EL PADRE */
            wait(NULL);
            printf("EL padre lee de la tuberia...\n");
            printf("Tamaño en bytes del mensaje: %d\n", lencadena);
            read(fd[0], buffer, lencadena);
            printf("Datos leidos: %s\n", buffer);
         }
    return 0;
}
