#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <string.h>

int main(void){

  pid_t pid;
  int fd[2];
  char buffer[100];
  size_t return_write;
  char *saludo="hola proceso padre";
  int i = 0;
  int bytes_leidos;

  
  pipe(fd);
  pid = fork();


  switch (pid) {
    case -1:
      return -1;
    case 0: /*PROCESO HIJO*/
      close(fd[0]);
      printf("El hijo escribe en la tuberia...%d\n", strlen(saludo));
      write(fd[1], saludo, strlen(saludo)+1);
      close(fd[1]);
      break;
    default: /*PROCESO PADRE*/  
      close(fd[1]);
      wait(NULL);
      printf("El padre lee de la tuberia ...\n");
      /*read(fd[0], buffer, 100);*/
      while ((bytes_leidos = read(fd[0], buffer + i, 1)) == 1){
      /*while ((bytes_leidos = read(fd[0], buffer + i++, 1)) == 1); tambien nos serviria este bucle */
        i++;
        /*printf("%c - Bytes Leidos %d\n", buffer[i++], bytes_leidos); para comprobar que hace con cada byte*/
      }

      printf("Datos leidos: \n%s\n", buffer);
  }
  return 0;
}
