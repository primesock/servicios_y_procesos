#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <string.h>
int main(void){
    
    pid_t pid;
    int fd[2];
    char buffer[100];
    char *saludo = "Hola proceso padre";
    int i = 0;
    int bytes_leidos;

    pipe(fd);
    pid = fork();

    switch(pid){
        case -1:
            return -1;
        case 0: //PROCESO HIJO
            close(fd[0]);
            printf("El hijo escribe en la tuberia...");
            write(fd[1], saludo, strlen(saludo));
            close(fd[0]);
            break;
        default: //PROCESO PADRE
            wait(NULL);
            printf("El padre lee de la tuberia...\n");
            while(bytes_leidos = read(fd[0], buffer +  i++, 1)) == 1 {
                printf("Datos leidos: \n%d\n", buffer);

