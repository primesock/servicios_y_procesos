#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(void){
    pid_t pidhijo;
    pid_t pidnieto;

    printf("Soy el abuelo %d\n", getpid());
    pidhijo = fork();
    if (pidhijo == -1){
        printf("Error creando el proceso hijo");
        return -1;
    }
    if (pidhijo == 0){
        printf("Soy el hijo  %d y mi padre es %d\n", getpid(), getppid());
        pidnieto = fork();
        ;
        if (pidnieto == -1){
            printf("Error creando el proceso nieto");
        }
        if (pidnieto == 0){
            printf("Soy el nieto %d y mi padre es %d\n", getpid(), getppid());
        }
    }
    wait(NULL);
    printf("PID actual : %d\n", getpid());
    return 0;
}
