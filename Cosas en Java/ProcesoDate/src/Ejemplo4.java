import java.io.*;
public class Ejemplo4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Runtime runtime = Runtime.getRuntime();
		String cmd = "terminal /C date +%d/%m/%Y%t";
		String newDate = "01-10-2017" + System.getProperty("line.separator");
		Process process = null;
		try {
			process = runtime.exec(cmd);
			//escritura — envia entrada a DATE
			OutputStream os = process.getOutputStream();
			os.write(newDate.getBytes());
			os.flush(); //vacia el buffer de salida
			//lectura — obtiene la salida de DATE
			InputStream is = process.getInputStream();
			BufferedReader br = new BufferedReader(
					new InputStreamReader (is));
			String linea;
			while((linea = br.readLine()) != null)
				System.out.println(linea);
			br.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		// COMPROBACION DE ERROR - 0 bien 1 -mal
		int exitVal;
		try {
			exitVal = process.waitFor();
			System.out.println("Valor de Salida: " + exitVal);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}