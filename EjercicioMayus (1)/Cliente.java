public class Cliente {
	    
    public static void main(String[] args) {
    	
    	boolean seguir = true;//bandera para controlar ciclo del programa
        Socket cliente = null;
        BufferedReader entrada = null;
        PrintWriter salida = null;
        try {            
            System.out.println("Cliente> Inicio");  

            while(seguir){//ciclo repetitivo                                
                cliente = new Socket("localhost", 4000);//abre socket                
                //Para leer lo que envie el servidor      
                entrada = new BufferedReader( new InputStreamReader(cliente.getInputStream()));                
                //para imprimir datos del servidor
            	salida = new PrintWriter(new OutputStreamWriter(cliente.getOutputStream()), true);
                //Para leer lo que escriba el usuario            
                BufferedReader brRequest = new BufferedReader(new InputStreamReader(System.in));            
                System.out.println("Cliente> Escriba comando");                
                //captura comando escrito por el usuario
                String request = brRequest.readLine();                
                //manda peticion al servidor
                salida.println(request);
                //captura respuesta e imprime
                String respuesta = entrada.readLine();
                if(request.equals("*")){//terminar aplicacion
                    seguir = false;                  
                    System.out.println("Cliente> Fin de programa");  
                }
                else {
                	System.out.println(respuesta);
                }
            }//end while                                    
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (entrada != null) {
				try {
					entrada.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (salida != null) {
				salida.close();
			}
			if (cliente != null) {
				try {
					cliente.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
