public class Servidor {

    public static void main(String[] args) {
        
    	ServerSocket servidor = null;
    	BufferedReader entrada = null;
    	PrintWriter salida = null;
        Socket cliente = null;

    	
        try {
        	
            //Socket de servidor para esperar peticiones de la red
        	servidor = new ServerSocket(4000);
        	
            System.out.println("Servidor> Servidor iniciado");    
            System.out.println("Servidor> En espera de cliente...");    
            //Socket de cliente
            boolean seguir = true;
            while(seguir){
                //en espera de conexion, si existe la acepta
                cliente = servidor.accept();
                //Para leer lo que envie el cliente
                entrada = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
                //para imprimir datos de salida                
            	salida = new PrintWriter(new OutputStreamWriter(cliente.getOutputStream()), true);
                //se lee peticion del cliente
                String request = entrada.readLine();
                if (!request.equals("*")) {
                	System.out.println("Cliente> petición [" + request +  "]");
                    //Se imprime en consola "servidor"
                    System.out.println("Servidor> \"" + request + "\"");
                    //se imprime en cliente
                    salida.flush();//vacia contenido
                    salida.println(request.toUpperCase());
                }
                else {
                	seguir = false;
                }
            }    
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (entrada != null) {
				try {
					entrada.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (salida != null) {
				salida.close();
			}
			if (cliente != null) {
				try {
					cliente.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (servidor != null) {
				try {
					servidor.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}