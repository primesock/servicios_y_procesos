package fumadores;

import java.util.Random;

public class agente extends Thread{
	
	private static final Random r = new Random();
	mesa mesa;
	
	public agente(mesa mesa) {
		this.mesa = mesa;
	}
	@Override
	public void run() {
		int i1;
		int i2;
		while (true) {
			i1 = r.nextInt(3);
			do {
				i2 = r.nextInt(3);
			} while (i2 != i1);
			mesa.depositar(Ingrediente.values()[1], Ingrediente.values()[2]);
		}
	}
}
