package fumadores;

public class mainClass {

	public static void main(String[] args) {
		mesa m = new mesa();
		fumador f1 = new fumador(Ingrediente.PAPEL, m);
		fumador f2 = new fumador(Ingrediente.TABACO, m);
		fumador f3 = new fumador(Ingrediente.CERILLAS, m);
		agente a = new agente();
		f1.start();
		f2.start();
		f3.start();
		a.start();
		
		try {
			f1.join();
			f2.join();
			f3.join();
			a.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
