package fumadores;
/* 
 * Donde se puedan depositar o retirar ingredientes
 */
public class mesa {
	Ingrediente i1;
	Ingrediente i2;
	
	public synchronized void depositar(Ingrediente i1, Ingrediente i2) {
		this.i1 = i1;
		this.i2 = i2;
		System.out.println("El agente deposita " + i1 + " y " + i2);
		notifyAll();
	}
	
	public synchronized void fumar(Ingrediente i) {
		while (i == i1 || i == i2) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("El fumador con" + i + " esta liando un cigarro");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("El fumador con " + i + " esta fumando");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("El fumador con " + i + " ha terminado de fumar");
		i1 = i2 = null;
		notifyAll();
		
	}
}
