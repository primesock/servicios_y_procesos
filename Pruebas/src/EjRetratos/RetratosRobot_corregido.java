package EjRetratos;

import java.util.Scanner;

class retrato_corregido {
	static private String [][] rasgos = {
			{" WWWWWWWWW"," |||||||||", " |'''''''|"," \\\\///////"}, 
			{" |  O O  |", " |-(· ·)-|", " |-(o o)-|", " |  \\ /  |"},
			{"@    J    @", "{   ''    }", "[    j    ]", "<    -    >"},
			{" |  ===  |", " |   -   |", " |  ___  |", "|  ---  |"},
			{" \\______/", " \\,,,,,,/"}
			};
	
	private int pelo;
	private int ojos;
	private int nariz;
	private int boca;
	private int barbilla;
	
	public retrato_corregido(int pelo, int ojos, int nariz, int boca, int barbilla) throws Exception {
		
		if (pelo < 0 || pelo >= rasgos[0].length)
			throw new Exception("Opcion de pelo no valida");
		if (ojos < 0 || ojos >= rasgos[1].length)
			throw new Exception("Opcion de pelo no valida");
		if (nariz < 0 || nariz >= rasgos[2].length)
			throw new Exception("Opcion de pelo no valida");
		if (boca < 0 || boca >= rasgos[3].length)
			throw new Exception("Opcion de pelo no valida");
		if (barbilla < 0 || barbilla >= rasgos[4].length)
			throw new Exception("Opcion de pelo no valida");
		
		this.pelo = pelo;
		this.ojos = ojos;
		this.nariz = nariz;
		this.boca = boca;
		this.barbilla = barbilla;
	}
	
	@Override
	public String toString() {
		return rasgos[0][pelo] + System.getProperty("line.separator") +
				rasgos[1][ojos] + System.getProperty("line.separator") +
				rasgos[2][nariz] + System.getProperty("line.separator") +
				rasgos[3][boca] + System.getProperty("line.separator") +
				rasgos[4][barbilla] + System.getProperty("line.separator");
	}
	
	public static void mostrarOpciones(int rasgo) {
		for (int i=0; i<rasgos[rasgo].length; i++) {
			System.out.println(i + " - " + rasgos[rasgo][i]);
		}
	}
		
}

public class RetratosRobot_corregido {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		int opcionPelo, opcionOjos, opcionNariz, opcionBoca, opcionBarbilla;
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Elige una opcion para el pelo: ");
		retrato_corregido.mostrarOpciones(0);
		opcionPelo = teclado.nextInt();
		
		System.out.println("Elige una opcion para los ojos: ");
		retrato_corregido.mostrarOpciones(1);
		opcionOjos = teclado.nextInt();
		
		System.out.println("Elige una opcion para la nariz: ");
		retrato_corregido.mostrarOpciones(2);
		opcionNariz = teclado.nextInt();
		
		System.out.println("Elige una opcion para la boca: ");
		retrato_corregido.mostrarOpciones(3);
		opcionBoca = teclado.nextInt();
		
		System.out.println("Elige una opcion para la barbilla: ");
		retrato_corregido.mostrarOpciones(4);
		opcionBarbilla = teclado.nextInt();
		
		retrato_corregido r = new retrato_corregido(opcionPelo, opcionOjos, opcionNariz, opcionBoca, opcionBarbilla);
		System.out.print(r);
		
	}

}
