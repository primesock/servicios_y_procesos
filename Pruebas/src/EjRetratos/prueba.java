package EjRetratos;

import java.util.Scanner;

public class prueba{
	public static void main(String[] args) {
		String valor1, valor2;
		Scanner teclado= new Scanner(System.in);
		System.out.println("Introduce valores");
		valor1 = teclado.nextLine();
		valor2 = teclado.nextLine();
        //Pasamos el binario a decimal, base 10
        int num1 = Integer.parseInt(valor1, 2);
        System.out.println(num1);
        int num2 = Integer.parseInt(valor2, 2);
        System.out.println(num2);
        //Suma decimal
        int suma = num1 + num2;
        System.out.println(suma);
        //Pasamos a binario
        String sumabin = Integer.toBinaryString(suma);
        System.out.println("Suma: " + sumabin);
        
    }
}