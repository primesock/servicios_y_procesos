package EjRetratos;

public class rasgos_cara {
	private String pelo1 = " WWWWWWWWW";
	private String pelo2 = " |||||||||";
	private String pelo3 = " |'''''''|";
	private String pelo4 = " \\\\///////";
	private String ojos1 = " |  O O  |";
	private String ojos2 = " |-(· ·)-|";
	private String ojos3 = " |-(o o)-|";
	private String ojos4 = " |  \\ /  |";
	private String nariz1 = "@    J    @";
	private String nariz2 = "{   ''    }";
	private String nariz3 = "[    j    ]";
	private String nariz4 = "<    -    >";
	private String boca1 = " |  ===  |";
	private String boca2 = " |   -   |";
	private String boca3 = " |  ___  |";
	private String boca4 = " |  ---  |";
	private String barbilla = "  \\_____/ ";
	
	public void menu() {
		System.out.println("Rasgos: \n" + pelo1 + "   " + pelo2 + "   " + pelo3 + "   " + pelo4 + "\n" 
										+ ojos1 + "   " + ojos2 + "   " + ojos3 + "   " + ojos4 + "\n" 
										+ nariz1 + "  " + nariz2 + "  " + nariz3 + "  " + nariz4 + "\n" 
										+ boca1 + "   " + boca2 + "   " + boca3 + "   " + boca4 + "\n"
										+ barbilla + "   " + barbilla + "   " + barbilla + "   " + barbilla + "\n"
										+ "-- n. 1 --" + "   " + "-- n. 2 --" + "   " + "-- n. 3 --" + "   " + "-- n. 4 --" + "\n");
	}
	
	public String getPelo1() {
		return pelo1;
	}
	public String getPelo2() {
		return pelo2;
	}
	public String getPelo3() {
		return pelo3;
	}
	public String getPelo4() {
		return pelo4;
	}
	public String getOjos1() {
		return ojos1;
	}
	public String getOjos2() {
		return ojos2;
	}
	public String getOjos3() {
		return ojos3;
	}
	public String getOjos4() {
		return ojos4;
	}
	public String getNariz1() {
		return nariz1;
	}
	public String getNariz2() {
		return nariz2;
	}
	public String getNariz3() {
		return nariz3;
	}
	public String getNariz4() {
		return nariz4;
	}
	public String getBoca1() {
		return boca1;
	}
	public String getBoca2() {
		return boca2;
	}
	public String getBoca3() {
		return boca3;
	}
	public String getBoca4() {
		return boca4;
	}
	public String getBarbilla() {
		return barbilla;
	}
	
}
