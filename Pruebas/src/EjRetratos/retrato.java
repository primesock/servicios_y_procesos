package EjRetratos;

class retrato extends rasgos_cara {
	private int pelo;
	private int ojos;
	private int nariz;
	private int boca;
	
	public retrato(int pelo, int ojos, int nariz, int boca) {
		super();
		this.pelo = pelo;
		this.ojos = ojos;
		this.nariz = nariz;
		this.boca = boca;
	}

	public void imprimir() {
		System.out.println("Retrato:");
		switch (pelo) {
		case 1:
			System.out.println(getPelo1());
			break;
		case 2:
			System.out.println(getPelo2());
			break;
		case 3:
			System.out.println(getPelo3());
			break;
		case 4:
			System.out.println(getPelo4());
			break;
		}
		switch (ojos) {
		case 1:
			System.out.println(getOjos1());
			break;
		case 2:
			System.out.println(getOjos2());
			break;
		case 3:
			System.out.println(getOjos3());
			break;
		case 4:
			System.out.println(getOjos4());
			break;
		}
		switch (nariz) {
		case 1:
			System.out.println(getNariz1());
			break;
		case 2:
			System.out.println(getNariz2());
			break;
		case 3:
			System.out.println(getNariz3());
			break;
		case 4:
			System.out.println(getNariz4());
			break;
		}
		switch (boca) {
		case 1:
			System.out.println(getBoca1());
			break;
		case 2:
			System.out.println(getBoca2());
			break;
		case 3:
			System.out.println(getBoca3());
			break;
		case 4:
			System.out.println(getBoca4());
			break;
		}
		System.out.println(getBarbilla());
	}
}