import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Cliente {

	public static void main(String[] args) {
		
		MarcoCliente mimarco = new MarcoCliente();

	}

}

class MarcoCliente extends JFrame {

	public MarcoCliente() {
		setTitle("Cliente");
		setBounds(600, 300, 280, 350);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		PanelCliente panel = new PanelCliente();
		add(panel);
		setVisible(true);
	}
}

class PanelCliente extends JPanel implements Runnable{
	private JLabel nick;
	private JTextField mensaje,ip;
	private JButton boton;
	private JTextArea areachat;
	private String nombre,aux;
	public PanelCliente() {
		System.out.print("Escriba su nick: ");
		Scanner s = new Scanner (System.in);
		nombre = s.nextLine();
		nick= new JLabel(nombre);
		add(nick);
		ip = new JTextField(8);
		add(ip);
		areachat = new JTextArea(12, 20);
		areachat.setEditable(false);
		try {
			InetAddress ip_local = InetAddress.getLocalHost();
			String aux = ip_local.getHostAddress();
			areachat.setText("Tu ip es: "+aux);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		add(areachat);
		mensaje = new JTextField(20);
		add(mensaje);
		boton = new JButton("Enviar");
		EventoBoton evento = new EventoBoton();
		boton.addActionListener(evento);
		add(boton);
		Thread mihilo = new Thread(this);
		mihilo.start();
	}

	private class EventoBoton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Socket mishocket = new Socket("192.168.1.135", 9999);
				Paquete_Envio paquete = new Paquete_Envio(nick.getText(),ip.getText(),mensaje.getText());
				ObjectOutputStream flujo_salida = new ObjectOutputStream(mishocket.getOutputStream());
				flujo_salida.writeObject(paquete);
				flujo_salida.flush();
				flujo_salida.close();
				mishocket.close();
				areachat.append("\nYo: "+mensaje.getText());
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			ServerSocket servidor_cliente = new ServerSocket(9090);
			Socket cliente;
			Paquete_Envio paquete_Recibido;
			while (true) {
				cliente = servidor_cliente.accept();
				ObjectInputStream flujo_entrada = new ObjectInputStream(cliente.getInputStream());
				paquete_Recibido = (Paquete_Envio) flujo_entrada.readObject();
				areachat.append("\n"+paquete_Recibido.getNick()+": "+paquete_Recibido.getMensaje());
			}
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}


}

class Paquete_Envio implements Serializable{
	private String nick,ip,mensaje;
	
	public Paquete_Envio(String nick, String ip, String mensaje) {
		this.nick=nick;
		this.ip=ip;
		this.mensaje=mensaje;
	}
	
	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
