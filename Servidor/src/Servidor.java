import java.io.*;
import java.net.*;

public class Servidor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ServerSocket servidor = null;
		BufferedReader entrada = null;
		PrintWriter salida = null;
		Socket cliente = null;
		
		try {
			servidor = new ServerSocket(6000);
			cliente = servidor.accept();
			
			entrada = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
			salida = new PrintWriter(new OutputStreamWriter(cliente.getOutputStream()), true);
			// true para que no quede nada en el buffer de salida
			
			String linea;
			
			do {
				linea = entrada.readLine();
				System.out.println(linea.toUpperCase());
			} while (!linea.equals("*"));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (entrada != null) {
				try {
					entrada.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (salida != null) {
				salida.close();
			}
			if (cliente != null) {
				try {
					cliente.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (servidor != null) {
				try {
					servidor.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
