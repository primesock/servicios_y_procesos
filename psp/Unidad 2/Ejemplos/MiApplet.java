package org.fp.jplb;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.util.Random;

public class MiApplet extends Applet implements Runnable {
	
	private Thread t = null;
	private float x;
	private float y;
	private float vx;
	private int radio;
	private BufferedImage buffer;
	private Graphics g1, g2;
	
	@Override
	public void init() {
		super.init();
		x = getWidth() / 2;
		y = getHeight() / 2;
		vx = 30;
		radio = 30;
		buffer = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		g1 = buffer.getGraphics();
		g2 = getGraphics();
	}
	
	@Override
	public void start() {
		t = new Thread(this);
		t.start();
	}
	
	@Override
	public void stop() {
		super.stop();
	}
	
	@Override
	public void destroy() {
		super.destroy();
	}
	
	public void paint() {
		//super.paint(g);
		this.g1.clearRect(0, 0, getWidth(), getHeight());
		this.g1.fillOval((int) x - radio, (int) y - radio, radio * 2, radio * 2);
		g2.drawImage(buffer, 0, 0, null);
	}
	
	@Override
	public void paint(Graphics g) {
		//super.paint(g);
		this.g1.clearRect(0, 0, getWidth(), getHeight());
		this.g1.fillOval((int) x - radio, (int) y - radio, radio * 2, radio * 2);
		g.drawImage(buffer, 0, 0, null);
	}

	@Override
	public void run() {
		long t0 = System.nanoTime(), t1, lapso;
		while (true) {
			lapso = (t1 = System.nanoTime()) - t0;
			t0 = t1;
			x += lapso * vx / 1000000000L;
			if (x >= getWidth() - radio) {
				x = getWidth() - radio;
				vx *= -1;
			}
			else if (x <= radio) {
				x = radio;
				vx *= -1;
			}
			paint();
		}
	}
}
